#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>


// warningfill()
// Rellena el arreglo de eventos de advertencia.
void warningfill(char *warning_events[]) {

   // Cantidad de operaciones.
   int num_events = 12;

   // Lognitud del evento mas grande que puede haber.
   int max_strlen = 27;

   int i;


   for (i = 0 ; i < num_events ; i++)
      warning_events[i] = malloc(max_strlen);

   warning_events[0] = "Communication Offline";
   warning_events[1] = "Communication error";
   warning_events[2] = "Low Cash alert";
   warning_events[3] = "Running Out of notes in cassette";
   warning_events[4] = "empty";
   warning_events[5] = "Service mode entered";
   warning_events[6] = "Service mode left";
   warning_events[7] = "device did not answer as expected";
   warning_events[8] = "The protocol was cancelled";
   warning_events[9] = "Low Paper warning";
   warning_events[10] = "Printer Error";
   warning_events[11] = "Paper-out condition";

}

// warningcheck()
// Chequea si un evento esta en la lista de eventos de advertencia.
int warningcheck(char *warning_events[], char *event) {

   // Cantidad de elementos en el arreglo de codigos.
   int num_events = 12;

   int i = 0;

   for (i = 0 ; i < num_events ; i++) {

      //printf("%s == %s\n", event, warning_events[i]);
      if (strcmp(event, warning_events[i]) == 0) {
         return 1;
      }

   }

   return 0;

}

// getsockline()
// Obtiene una linea del file descriptor del socket.
char * getsockline(int sockfd) {
   
   // Caracter a leer del buffer del socket.
   char *character;


   int bytes_read;

   char *line = NULL;

   int n = 1;
   
   // Reserva del espacio para leer la linea.
   line = (char *) malloc(n);

   while (1) {

      bytes_read = read(sockfd, &character, 1);

      if ((strcmp(character, "\n") != 0) && (bytes_read != 0)) {
         line = (char *) realloc(line, n);
         strcat(line, character);
      }
      else
         return line;

   }

}

// log_management.c
// Operaciones sobre la bitacora y manejo de eventos de advertencia.
void log_management(int sockfd, char *file_path) {

   // Condicion para tokenizar con strtok().
   const char eq[] = "=";

   // Condicion para tokenizar con strtok().
   const char o[] = "<";

   // Condicion para tokenizar con strtok().
   const char c[] = "/>";

   // Condicion para tokenizar con strtok().
   const char blank[] = " ";

   // Almacena cada token obtenido por strtok().
   char *token;

   // Almacena cada linea leida de la entrada estandar.
   char *line;

   // Cantidad de bytes a leer de la entrada estandar.
   // (Se coloca un valor inicial (10) y getline() se encara de modificar este 
   //  valor de forma dinamica en caso de ser necesario).
   size_t n = 10;

   // Bytes leidos en una linea de la entrada estandar.
   int bytes_read;

   // File descriptor del archivo "record.txt" (bitacora).
   FILE *fd;

   // Ruta de la bitacora.
   //char *file_path = "record.txt";

   // Modo en el que se abrira la bitacora.
   char *mode = "a";

   // Codigos de advertencia.
   char *warning_events[12];

   // Contador para el ciclo for.
   int i;

  
   // Inicializacion de los codigos de advertencia.
   warningfill(warning_events);

   // Apertura de la bitacora.
   fd = fopen(file_path, mode);

   if (fd == NULL) {
      fprintf(stderr, "Apertura de archivo fallida.\n");
      exit(1);
   }

   //while ((bytes_read = getline(&line, &n, stdin)) != -1) {
   while (1) {

      // Lectura caracter por caracter del buffer para obtener linea a linea
      // los datos que contiene el socket.
      line = getsockline(sockfd);

      // Parseo de la cabecera del mensaje XML:
      // <?xml version = "1.0"?>
      // <log_entry>
      token = strtok(line, "<?");
      token = strtok(token, "?>");

      bytes_read = getline(&line, &n, stdin);

      // El paquete no llego completo.
      if (bytes_read == -1)
        exit(0);

      token = strtok(line, o);
      token = strtok(token, c);

      // Formato para cada entrada de la bitacora.
      // <serial> <dia>.<mes>.<año> <hora>:<min> <atm> <evento>
      fprintf(fd, "%s ", "<serial>");

      // Parseo de las lineas que contienen parte de la informacion a guardar 
      // en la bitacora.
      for (i = 0 ; i < 7 ; i++) {

         bytes_read = getline(&line, &n, stdin);

         // El paquete no llego completo.
         if (bytes_read == -1)
            exit(0);

         token = strtok(line, eq);
         token = strtok(NULL, eq);
         token = strtok(token, c);

         if (i == 6) {

            // Se omite el primer caracter (espacio en blanco) del evento.
            memmove(token, token + 1, strlen(token));

            // Se detecta si el evento amerita enviar una advertencia.
            if (warningcheck(warning_events, token) == 1) {
               //ENVIAR MAIL Y TWITTEAR
               printf("TWEET\n");
            }

         } else
            token = strtok(token, blank);

         // Seleccion del formato en el que se imprimira en la bitacora.
         if (i == 3)
            fprintf(fd, "%s:", token);
         else
            fprintf(fd, "%s ", token);

      }

      // Parseo del cierre del log:
      // </log_entry>
      if ((bytes_read = getline(&line, &n, stdin)) != -1) {

         token = strtok(line, o);
         token = strtok(token, c);

         fprintf(fd, "\n");

      }

   }

   // Cierre de la bitacora.
   fclose(fd);

}
