#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <sys/types.h>

#define BUFFER_LEN 1024


// log_entry
// Estructura que define una entrada de log.
typedef struct log_entry {
   char *day;
   char *month;
   char *year;
   char *hour;
   char *min;
   char *atm;
   char *desc;
} log_entry;

// formatlog()
// Formatea en XML una entrada de log.
void formatlog(log_entry entry, char *data) {

   // Almacena la linea formateada.
   char xml_line[50];


   // Construccion del header del mensaje.
   strcpy(data, "<?xml version = \"1.0\"?>\n");
   strcat(data, "<log_entry>\n");

   sprintf(xml_line, "<day = %s/>\n", entry.day);
   strcat(data, xml_line);

   sprintf(xml_line, "<month = %s/>\n", entry.month);
   strcat(data, xml_line);

   sprintf(xml_line, "<year = %s/>\n", entry.year);
   strcat(data, xml_line);

   sprintf(xml_line, "<hour = %s/>\n", entry.hour);
   strcat(data, xml_line);

   sprintf(xml_line, "<min = %s/>\n", entry.min);
   strcat(data, xml_line);

   sprintf(xml_line, "<atm = %s/>\n", entry.atm);
   strcat(data, xml_line);

   sprintf(xml_line, "<desc = %s/>\n", entry.desc);
   strcat(data, xml_line);

   strcat(data, "</log_entry>");

}

// getlogentry()
// Obtiene linea por linea los datos de la entrada estandar, los formatea en
// XML y copia el resultado en <data>.
int getlog(char *data) {

   // Condicion para tokenizar con strtok().
   const char colon[] = ":";

   // Condicion para tokenizar con strtok().
   const char blank[] = " ";

   // Almacena cada token obtenido por strtok().
   char *token;

   // Almacena cada linea leida de la entrada estandar.
   char *line;

   // Cantidad de bytes a leer de la entrada estandar.
   // (Se coloca un valor inicial (10) y getline() se encara de modificar este 
   //  valor de forma dinamica en caso de ser necesario).
   size_t n = 10;

   // Bytes leidos en una linea de la entrada estandar.
   int bytes_read;

   // Estructura que almacena el log leido.
   log_entry entry;


   // Reserva del espacio para leer la linea.
   line = (char *)malloc(n);

   if ((bytes_read = getline(&line, &n, stdin)) != -1) {

      // Parseo de la linea y almacenamiento de los datos del log en la
      // estructura <entry>.
      token = strtok(line, colon);
      entry.day = token;

      token = strtok(NULL, colon);
      entry.month = token;

      token = strtok(NULL, colon);
      entry.year = token;

      token = strtok(NULL, colon);
      entry.hour = token;

      token = strtok(NULL, colon);
      entry.min = token;

      token = strtok(NULL, blank);
      entry.atm = token;

      token = strtok(NULL, "");

      // Se elimina el ultimo caracter (\n) de la descripcion del evento.
      token[strlen(token) - 1] = 0;
      entry.desc = token;

      // Formateo de cada linea.
      formatlog(entry, data);
      // printf("%s\n", data); 

   }

   return strlen(line);

}


int main(int argc, char *argv[]) {

   // File Descriptor del socket.
   int sockfd;

   // Estructura que contendrá la direccion IP y numero de puerto del servidor.
   struct sockaddr_in serv_addr;

   // Hostname del servidor.
   struct hostent *serv_entry;

   // Puerto del servidor.
   int serv_port;

   // Hostname del servidor,
   char *serv_hostname;

   // Cantidad de bytes escritos.
   int numbytes;

   // Mensaje que contendrá el log a enviar.
   char data[300];


   // Usage.
   if ((argc < 5) || (argc > 7)) {
      fprintf(stderr,"uso: %s -d <nombre_módulo_central> -p <puerto_svr_s> [-l <puerto_local>]\n", argv[0]);
      exit(1);
   }

   serv_port = atoi(argv[4]);
   serv_hostname = argv[2];

   // Obtencion de las IP del servidor mediante su hostname.
   if ((serv_entry = gethostbyname(serv_hostname)) == NULL) {
      perror("Resolucion del hostname fallida.");
      exit(1);
   }

   // Creacion del socket para usar TCP sobre IPv4.
   if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
      perror("Creacion del socket fallida.");
      exit(2);
   }


   /*
      Configuracion de red de la estructura serv_addr.
   */

   // Establecimiento de IPV4 como la familia de protocolos a usar en la estructura.
   serv_addr.sin_family = AF_INET;          
   
   // Establecimiento del puerto a utilizar. 
   // htons() convierte el unsigned short SERVER_PORT del host order al network 
   // order. 
   serv_addr.sin_port = htons(serv_port); 

   // Asignacion de la primera direccion IP de la lista h_addr_list.
   serv_addr.sin_addr = *((struct in_addr *)serv_entry->h_addr);
   // memcpy(&serv_addr.sin_addr, serv_entry->h_addr, serv_entry->h_length);   

   // Relleno con ceros el resto de la estructura.
   bzero(&(serv_addr.sin_zero), 8);         

   printf("Estableciendo conexion\n");

   // Establecimiento de la conexion.
   if (connect(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0) {
      perror("Establecimiento de conexion fallido.");
      exit(2);      
   }

   printf("Conexion establecida\n");

   // Envio de datos mientras haya escritura en la entrada estandar.
   printf("Enviando datos\n");

   // Se mantiene el socket abierto y se envian datos siempre que haya escritura
   // en el socket. De lo contrario, mantiene el socket abierto esperando 
   // datos.
   while (1) {
      if (getlog(data) > 0) {
         printf("%s\n", data);      
         if ((numbytes = write(sockfd, data, strlen(data))) == -1) {
            perror("Envio de datos fallido.");
            exit(2);
         }      
      }
   }

   // Reporte de envio.
   printf("Enviados %d bytes a %s\n", numbytes, inet_ntoa(serv_addr.sin_addr));

   // Cierre del socket.
   close(sockfd);
   exit (0);

}
