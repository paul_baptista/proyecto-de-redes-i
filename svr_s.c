#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
//Funciones y constantes estándares de Unix
#include <unistd.h>
//Definiciones para operaciones con basese de datos de red.
#include <netdb.h>
#include <pthread.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include "log_management.c"

#define BUFFER_LEN 1

void * serverthread(void *param);

typedef struct threadinf {
   int mysockfd;
   struct sockaddr_in my_client_addr;
} threadinf;


int main(int argc, char *argv[]) {

   int sockfd, newsockfd;

   int server_port;

   int max_conn = 1000;

   //Estructura que contendrá mi dirección y puerto.
   struct sockaddr_in serv_addr;

   //Estructura que contendrá la direccion IP y numero de puerto del cliente
   struct sockaddr_in client_addr;  

   threadinf *newsock_threadinf;
   pthread_t *tid = (pthread_t *) malloc(sizeof(pthread_t)*max_conn);
   int thread_cont = 0;
   
   //addr_len contendrá el tamaño de la estructura sockaddr_in y numbytes el
   //numero de bytes recibidos 
   int addr_len, numbytes;

   // char buf[BUFFER_LEN]; //Buffer de recepción

   char *log_file;

   // Usage del script.
   // Implementar Getopt
   if (argc != 5) {
      fprintf(stderr,
               "\nuso: %s -l <puerto_svr_s> -b <archivo_bitácora>\n", argv[0]);
      exit(1);
   }

   server_port = atoi(argv[2]);

   log_file = argv[4];

   //Creación del socket, para que use TCP sobre IPv4.
   if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
      perror("socket");
      exit(1);
   }

   /* Se establece la estructura serv_addr para luego llamar a bind() */
   
   serv_addr.sin_family = AF_INET; //Se establece IPV4 como la familia de
                                 //protocolos a usar en la estructura.

   serv_addr.sin_port = htons(server_port); //Se establece el puerto con el que
                                          //trabajará. htons() convierte el
                                          //unsigned short server_port del host
                                          //order al network order
   
   serv_addr.sin_addr.s_addr = INADDR_ANY;  //Se asocia la estructura a cualquier
                                          //ip.
   
   bzero(&(serv_addr.sin_zero), 8);         //Rellena con ceros el resto de la
                                          //estructura

   
   // Se asocia el socket a la dirección y puerto de serv_addr
   printf("Asignado direccion al socket ....\n");
   if (bind(sockfd,(struct sockaddr *)&serv_addr,sizeof(struct sockaddr)) < 0) {
      perror("bind");
      exit(2);
   }

   //Se esperan conexiones entrantes.
   if (listen(sockfd, max_conn) < 0) {
      perror("Error al esperar conexiones");
      exit(1);
   }

   addr_len = sizeof(client_addr);

   printf("Esperando conexiones entrantes ....\n");
   while (thread_cont < max_conn){
      printf("pepe\n");
      if ( ( newsockfd = 
               accept(sockfd,(struct sockaddr *)&client_addr,
                                       (socklen_t *)&addr_len) ) < 0 ){
         perror("Error al aceptar conexión");
         exit(1);
      }

      printf("pepe2\n");
      newsock_threadinf = (threadinf *) malloc(sizeof(threadinf));
      newsock_threadinf->mysockfd = newsockfd;
      newsock_threadinf->my_client_addr = client_addr;

      pthread_create(&(tid[thread_cont]), NULL, serverthread, (void *) newsock_threadinf);

      thread_cont++;

   }

   close(sockfd);
   exit (0);

}


void * serverthread(void * param) {
   char buf;

   char *line;

   int readed = 0;
   int iMode = 0;
   
   threadinf *my_inf;
   
   my_inf = (threadinf *) param;

   // ioctl(my_inf->mysockfd, FIONBIO, &iMode);  

   while (1){
      // printf("Esperando datos ....\n");
      // bzero(buf, BUFFER_LEN);
      if ((readed=read(my_inf->mysockfd, &buf, 1)) < 0){
         perror("recvfrom");
         exit(1);
      }

      if (readed==0){
         printf("connection closed\n");
         break;
      }

      printf("%c",buf);
   }
   pthread_exit(0);
} 